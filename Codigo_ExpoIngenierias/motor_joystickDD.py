#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy
myJoy=Joy()
myJoy.buttons=[255,255]
myJoy.axes=[0,0,0,0,0]

def callback(data):
    global myJoy
    myJoy=data

def talker():
    global myJoy
    rospy.init_node('motor_joystick', anonymous=True)
    pub_wr = rospy.Publisher('arduino/wr', Int32, queue_size=10)
    pub_wl = rospy.Publisher('arduino/wl', Int32, queue_size=10)
    rospy.Subscriber("joy", Joy, callback)
    rate = rospy.Rate(100) # 10hz
    r = 0.078
    L = 0.387
    wl = Int32()
    wr = Int32()


    while not rospy.is_shutdown():
        ##w=(myJoy.axes[0])  #axes[0] is left and right
        ##v=(myJoy.axes[1])  #axes[1] is up and down
        #####TODO:Functions to compute wr and wl here.
       ## wr = ((2*v)+(w*L))/(2*r) *21.25
        ## wl = ((2*v)-(w*L))/(2*r) *21.25
        
        ## if wr >= 255.0 : wr = 255.0
        ## if wr <= -255.0 : wr = -255.0
        ## if wl >= 255.0 : wl = 255.0
        ## if wl <= -255.0 : wl = -255.0    

        wl=(myJoy.axes[1])*255.0
        wr=(myJoy.axes[4])*255.0  
        
        pub_wr.publish(wr) #TODO: Don't forget to map this to the range -255 to 255
        pub_wl.publish(wl)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
