1) Upload the code to the Arduino using the Arduino IDE.

2) Start rosserial in the PC.
    $ rosrun rosserial_python serial_node.py /dev/ttyACM03) 

3) Publish some data  to the  /arduino/motor_vel topic. (You can try values from –255 to 255), the rotation direction should change if the sign changes.
    $ rostopic pub /arduino/motor_vel std_msgs/Int32 "data: 100" 
