#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import UInt16
from sensor_msgs.msg import Joy
myJoy=Joy()
myJoy.buttons=[255,255]
myJoy.axes=[0,0]

def callback(data):
    global myJoy
    myJoy=data

def talker():
    global myJoy
    rospy.init_node('servo_joystick', anonymous=True)    
    pub = rospy.Publisher('servo', UInt16, queue_size=10)
    rospy.Subscriber("joy", Joy, callback)
    rate = rospy.Rate(100) # 10hz
    servo=UInt16()

    while not rospy.is_shutdown():
        servo=(myJoy.axes[0]+1)*90
        pub.publish(servo)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
