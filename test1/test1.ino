// Define the pins of the first H Bridge:
#define H1enA 7
#define H1in1 6
#define H1in2 5
#define H1in3 4
#define H1in4 3
#define H1enB 2

// Define the pins of the second H Bridge:
#define H2enA 13
#define H2in1 12
#define H2in2 11
#define H2in3 10
#define H2in4 9
#define H2enB 8

// Define the button of the switch to enable the manual control:
#define button 48

// Motors speed variables, for progresive speed:
int motorSpeedA = 0;      // First motor, first H bridge
int motorSpeedB = 0;      // Second motor, first H bridge
int motorSpeedC = 0;      // First motor, second H bridge
int motorSpeedD = 0;      // Second motor, second H bridge
boolean enable = false;   // Enable the manual control

void setup() {
  pinMode(H1enA, OUTPUT);
  pinMode(H1enB, OUTPUT);
  pinMode(H1in1, OUTPUT);
  pinMode(H1in2, OUTPUT);
  pinMode(H1in3, OUTPUT);
  pinMode(H1in4, OUTPUT);
  pinMode(H2enA, OUTPUT);
  pinMode(H2enB, OUTPUT);
  pinMode(H2in1, OUTPUT);
  pinMode(H2in2, OUTPUT);
  pinMode(H2in3, OUTPUT);
  pinMode(H2in4, OUTPUT);
  pinMode(button, INPUT_PULLUP);    // Using a pullup instead of having the physical resistance in the circuit
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize digital pin LED_BUILTIN as an output.   
}

void loop() {
  int xAxis = analogRead(A0);       // Read Joysticks X-axis
  int yAxis = analogRead(A1);       // Read Joysticks Y-axis
  int push = digitalRead(button);   // Presssing the button of the joystick
  int oldPush = 1;                  // Old state of the button

  // Detect a push of the joystick button (active on low, because the pullup)
  if ( push == LOW && oldPush == HIGH) {
    enable = !enable;
    delay(100);
  }

  oldPush = push;
  
  if ( enable ) {

    // Turn on the LED
    digitalWrite(LED_BUILTIN, HIGH);
    
    // Y-axis used for forward and backward control
    if ( yAxis < 470 || yAxis > 550) {
      // Backward Control:
      if (yAxis < 470) {
        // Set Motor A backward
        digitalWrite(H1in1, LOW);
        digitalWrite(H1in2, HIGH);
        // Set Motor C backward
        digitalWrite(H2in1, HIGH);
        digitalWrite(H2in2, LOW);
        // Set Motor B backward
        digitalWrite(H1in3, LOW);
        digitalWrite(H1in4, HIGH);
        // Set Motor D backward
        digitalWrite(H2in3, HIGH);
        digitalWrite(H2in4, LOW);
        // Convert the declining Y-axis readings for going backward from 470 to 0 into 0 to 255 value for the PWM signal for increasing the motor speed
        motorSpeedA = map(yAxis, 470, 0, 0, 255);
        motorSpeedB = map(yAxis, 470, 0, 0, 255);
        motorSpeedC = map(yAxis, 470, 0, 0, 255);
        motorSpeedD = map(yAxis, 470, 0, 0, 255);
      }
      // Forward Control:
      else if (yAxis > 550) {
        // Set Motor A forward
        digitalWrite(H1in1, HIGH);
        digitalWrite(H1in2, LOW);
        // Set Motor C forward
        digitalWrite(H2in1, LOW);
        digitalWrite(H2in2, HIGH);
        // Set Motor B forward
        digitalWrite(H1in3, HIGH);
        digitalWrite(H1in4, LOW);
        // Set Motor D forward
        digitalWrite(H2in3, LOW);
        digitalWrite(H2in4, HIGH);
        // Convert the increasing Y-axis readings for going forward from 550 to 1023 into 0 to 255 value for the PWM signal for increasing the motor speed
        motorSpeedA = map(yAxis, 550, 1023, 0, 255);
        motorSpeedB = map(yAxis, 550, 1023, 0, 255);
        motorSpeedC = map(yAxis, 550, 1023, 0, 255);
        motorSpeedD = map(yAxis, 550, 1023, 0, 255);
      }
    }
  
    // X-axis used for left and right control
    else if (xAxis < 470 || xAxis > 550 ) {
      // Left Control:
      if (xAxis < 470) {
        // Convert the declining X-axis readings from 470 to 0 into increasing 0 to 255 value
        int xMapped = map(xAxis, 470, 0, 0, 255);
        // Move to left - decrease left motor speed, increase right motor speed
        motorSpeedA = motorSpeedA + xMapped;
        motorSpeedB = motorSpeedB + xMapped;
        motorSpeedC = motorSpeedC + xMapped;
        motorSpeedD = motorSpeedD + xMapped;
        // Confine the range from 0 to 255
        if (motorSpeedA > 255) {
          motorSpeedA = 255;
          digitalWrite(H1in1, LOW);
          digitalWrite(H1in2, HIGH);
        }
        if (motorSpeedB > 255) {
          motorSpeedB = 255;
          digitalWrite(H1in3, HIGH);
          digitalWrite(H1in4, LOW);
        }
        if (motorSpeedC > 255) {
          motorSpeedC = 255;
          digitalWrite(H2in1, LOW);
          digitalWrite(H2in2, HIGH);
        }
        if (motorSpeedD > 255) {
          motorSpeedD = 255;
          digitalWrite(H2in3, HIGH);
          digitalWrite(H2in4, LOW);
        }
      }
      // Rigth Control:
      else if (xAxis > 550) {
        // Convert the increasing X-axis readings from 550 to 1023 into 0 to 255 value
        int xMapped = map(xAxis, 550, 1023, 0, 255);
        // Move right - decrease right motor speed, increase left motor speed
        motorSpeedA = motorSpeedA + xMapped;
        motorSpeedB = motorSpeedB + xMapped;
        motorSpeedC = motorSpeedC + xMapped;
        motorSpeedD = motorSpeedD + xMapped;
        // Confine the range from 0 to 255
        if (motorSpeedA > 255) {
          motorSpeedA = 255;
          digitalWrite(H1in1, HIGH);
          digitalWrite(H1in2, LOW);
        }
        if (motorSpeedB > 255) {
          motorSpeedB = 255;
          digitalWrite(H1in3, LOW);
          digitalWrite(H1in4, HIGH);
        }
        if (motorSpeedC > 255) {
          motorSpeedC = 255;
          digitalWrite(H2in1, HIGH);
          digitalWrite(H2in2, LOW);
        }
        if (motorSpeedD > 255) {
          motorSpeedD = 255;
          digitalWrite(H2in3, LOW);
          digitalWrite(H2in4, HIGH);
        }
      }
    }
    
  
    // If joystick stays in middle, the motors are not moving
    else {
      motorSpeedA = 0;
      motorSpeedB = 0;
      motorSpeedC = 0;
      motorSpeedD = 0;
    }
    
    // Prevent buzzing at low speeds (Adjust according to your motors. My motors couldn't start moving if PWM value was below value of 70)
    if (motorSpeedA < 70) {
      motorSpeedA = 0;
    }
    if (motorSpeedB < 70) {
      motorSpeedB = 0;
    }
    if (motorSpeedC < 70) {
      motorSpeedC = 0;
    }
    if (motorSpeedD < 70) {
      motorSpeedD = 0;
    }
  
    // Send the output to enable the motors:
    analogWrite(H1enA, motorSpeedA);  // Send PWM signal to motor A
    analogWrite(H1enB, motorSpeedB);  // Send PWM signal to motor B
    analogWrite(H2enA, motorSpeedC);  // Send PWM signal to motor C
    analogWrite(H2enB, motorSpeedD);  // Send PWM signal to motor D
  }

  else {
    // Turn off the built-in LED:
    digitalWrite(LED_BUILTIN, LOW);
  }

}
