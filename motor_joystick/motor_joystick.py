#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy
myJoy=Joy()
myJoy.buttons=[255,255]
myJoy.axes=[0,0]

def callback(data):
    global myJoy
    myJoy=data

def talker():
    global myJoy
    rospy.init_node('motor_joystick', anonymous=True)    
    pub = rospy.Publisher('arduino/motor_vel', Int32, queue_size=10)
    rospy.Subscriber("joy", Joy, callback)
    rate = rospy.Rate(100) # 10hz
    motor=Int32()

    while not rospy.is_shutdown():
        motor=(myJoy.axes[0])*255
        pub.publish(motor)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
