/* 
*   INSTRUCCIONES Y DESCRIPCIÓN PARA TEST2.INO

*   Este código funciona para mover cuatro motores, utilizando dos puentes H 
    (revisar en la definición de pines cómo conectar los puentes) y un Joystick
    (revisar en la definición de pines cómo conectar el joystick).
    
*   Para activar el control de los motores con el Joystick es necesario presionar
    el botón (palanca) y para desactivarlo es volviendo a presionarlo.

*   El Arduino recibe constantemente la lectura del Joystick en sus ejes X y Y
    mediante una lectura analógica, por lo que en ambos ejes los valores posibles
    son de 0 a 1023, siendo 512 cuando se encuentra en medio.
    El Arduino determina la posición del Joystick (arriba, abajo, izquierda o derecha)
    y con base en estrucutras de control (if-elseif) determina qué datos enviar 
    a los motores.
    
*   Para mover los motores se requiere enviar la señal a un Puente H, por lo que la
    instrucción "digitalWrite(H1in1, HIGH);" y "digitalWrite(H1in2, LOW);" harán que
    el primer motor del primer puente H se mueva en dirección positiva (avance adelante)
    y si se intercambian los valores HIGH y LOW entonces el motor se movería a atrás.
    
*   Para conseguir la rotación sobre su propio eje, dos motores se mueven hacia adelante
    y los otros dos hacia atrás. Ejemplo: para rotar a la izquierda, las ruedas de la 
    izquierda rotan hacia atrás y las ruedas de la derecha rotan hacia adelante. 
    
--------------------------------------------------------------------------------------------

*   A diferencia de test1.ino, este archivo permite controlar los motores mediante
    un teclado, haciendo uso de librerías de ROS y Python.
    
*   El control mediante el Joystick tiene prioridad, por lo que al presionar el 
    botón del Joystick los motores se detendrán y obedecerán al Joystick. Una vez
    que se vuelva a oprimir el botón, para desactivar el control mediante el 
    Joystick, los motores volverán a funcionar acorde a las instrucciones del
    teclado.
    
*   Los controles mediante el teclado son las teclas que se especifican al lanzar
    los nodos de ROS.
    
*   Los nodos de ROS a lanzar se pueden ejecutar con los comandos:
        roscore
        rosrun rosserial_python serial_node.py /dev/ttyACM0
        rostopic pub /cmd_vel geometry_msgs/Twist "linear:
                x: 1.0
                y: 0.0
                z: 0.0
            angular:
                x: 0.0
                y: 0.0
                z: 0.0"
    
*/
