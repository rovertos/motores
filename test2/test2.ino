/*
Esquema de motores:

  MOTOR_B ------- MOTOR_A
             | 
             |
             |
  MOTOR_C ------- MOTOR_D

*/

// Libraries
#include <ros.h>
#include <geometry_msgs/Twist.h>

// Define the pins of the first H Bridge:
#define H1enA 7
#define H1in1 6
#define H1in2 5
#define H1in3 4
#define H1in4 3
#define H1enB 2

// Define the pins of the second H Bridge:
#define H2enA 13
#define H2in1 12
#define H2in2 11
#define H2in3 10
#define H2in4 9
#define H2enB 8

// Define the Joystick manual control:
#define xAxis A0
#define yAxis A1
#define button 48

// Define the LED, indicates when is active the manual control:
#define LED 50

// Motors speed variables, for progresive speed:
int motorSpeedA = 0;      // First motor, first H bridge
int motorSpeedB = 0;      // Second motor, first H bridge
int motorSpeedC = 0;      // First motor, second H bridge
int motorSpeedD = 0;      // Second motor, second H bridge
int motorSpeedAR = 0;      // First motor, first H bridge
int motorSpeedBR = 0;      // Second motor, first H bridge
int motorSpeedCR = 0;      // First motor, second H bridge
int motorSpeedDR = 0;      // Second motor, second H bridge
int Mov_X = 0;            // The read from the Joystick in the X axis
int Mov_Y = 0;            // The read from the Joystick in the Y axis
int push = 0;             // To detect when pressing the button of the Joystick
int oldPush = 1;          // Previous state of the variable "push"
boolean enable = false;   // Enable the manual control

int PWMLinear;
int PWMAngular;
int MoveFwd;
int MoveBck;
int RotateL;
int RotateR;

//ROS Node Handler
ros::NodeHandle nh;

float linearX;
float angularZ;

//Funcion de callback
void messageCb( const geometry_msgs::Twist &vel_msg){
  linearX = vel_msg.linear.x;
  angularZ = vel_msg.angular.z;

  if(linearX > 1.0){
    linearX = 1.0;
  }
  if(angularZ > 1.0){
    angularZ = 1.0;
  }
  if(linearX < -1.0){
    linearX = -1.0;
  }
  if(angularZ < -1.0){
    angularZ = -1.0;
  }

  if(linearX > 0.0){
    MoveFwd = 1;
    MoveBck = 0;
    RotateL = 0;
    RotateR = 0;
  }
  if(angularZ > 0.0){
    MoveFwd = 0;
    MoveBck = 0;
    RotateL = 1;
    RotateR = 0;
  }
  if(linearX < 0.0){
    MoveFwd = 0;
    MoveBck = 1;
    RotateL = 0;
    RotateR = 0;
  }
  if(angularZ < 0.0){
    MoveFwd = 0;
    MoveBck = 0;
    RotateL = 0;
    RotateR = 1;
  }

  PWMLinear = abs (linearX * 255);
  PWMAngular = abs (angularZ * 255);
  
}

//Subscriber
ros::Subscriber<geometry_msgs::Twist> sub("/cmd_vel", &messageCb );

void readJoystick() {
  Mov_X = analogRead(xAxis);    // Read Joysticks X-axis
  Mov_Y = analogRead(yAxis);    // Read Joysticks Y-axis
  push = digitalRead(button);   // Presssing the button of the joystick

  if ( push == LOW && oldPush == HIGH) {
    enable = !enable;
    delay(100);
  }

  oldPush = push;
}

void moveForward() {
  // Set Motor A backward
  digitalWrite(H1in1, LOW);
  digitalWrite(H1in2, HIGH);
  // Set Motor C backward
  digitalWrite(H2in1, HIGH);
  digitalWrite(H2in2, LOW);
  // Set Motor B backward
  digitalWrite(H1in3, LOW);
  digitalWrite(H1in4, HIGH);
  // Set Motor D backward
  digitalWrite(H2in3, HIGH);
  digitalWrite(H2in4, LOW);
  // Convert the declining Y-axis readings for going backward from 470 to 0 into 0 to 255 value for the PWM signal for increasing the motor speed
  motorSpeedA = 255;
  motorSpeedB = 255;
  motorSpeedC = 255;
  motorSpeedD = 255;
}

void moveForward_Remote() {
  // Set Motor A forward
  digitalWrite(H1in1, HIGH);
  digitalWrite(H1in2, LOW);
  // Set Motor C forward
  digitalWrite(H2in1, LOW);
  digitalWrite(H2in2, HIGH);
  // Set Motor B forward
  digitalWrite(H1in3, HIGH);
  digitalWrite(H1in4, LOW);
  // Set Motor D forward
  digitalWrite(H2in3, LOW);
  digitalWrite(H2in4, HIGH);
  // Convert the declining Y-axis readings for going backward from 470 to 0 into 0 to 255 value for the PWM signal for increasing the motor speed
  motorSpeedAR = PWMLinear;
  motorSpeedBR = PWMLinear;
  motorSpeedCR = PWMLinear;
  motorSpeedDR = PWMLinear;
}

void moveBackward() {
  // Set Motor A forward
  digitalWrite(H1in1, HIGH);
  digitalWrite(H1in2, LOW);
  // Set Motor C forward
  digitalWrite(H2in1, LOW);
  digitalWrite(H2in2, HIGH);
  // Set Motor B forward
  digitalWrite(H1in3, HIGH);
  digitalWrite(H1in4, LOW);
  // Set Motor D forward
  digitalWrite(H2in3, LOW);
  digitalWrite(H2in4, HIGH);
  // Convert the increasing Y-axis readings for going forward from 550 to 1023 into 0 to 255 value for the PWM signal for increasing the motor speed
  motorSpeedA = 255;
  motorSpeedB = 255;
  motorSpeedC = 255;
  motorSpeedD = 255;
}

void moveBackward_Remote() {

    // Set Motor A backward
  digitalWrite(H1in1, LOW);
  digitalWrite(H1in2, HIGH);
  // Set Motor C backward
  digitalWrite(H2in1, HIGH);
  digitalWrite(H2in2, LOW);
  // Set Motor B backward
  digitalWrite(H1in3, LOW);
  digitalWrite(H1in4, HIGH);
  // Set Motor D backward
  digitalWrite(H2in3, HIGH);
  digitalWrite(H2in4, LOW);
  // Convert the increasing Y-axis readings for going forward from 550 to 1023 into 0 to 255 value for the PWM signal for increasing the motor speed
  motorSpeedAR = PWMLinear;
  motorSpeedBR = PWMLinear;
  motorSpeedCR = PWMLinear;
  motorSpeedDR = PWMLinear;
}

void moveLeft() {
    // Convert the increasing X-axis readings from 550 to 1023 into 0 to 255 value
  int xMapped = map(xAxis, 470, 0, 0, 255);
  // Move right - decrease right motor speed, increase left motor speed
  motorSpeedA = 256;
  motorSpeedB = 256;
  motorSpeedC = 256;
  motorSpeedD = 256;
  // Confine the range from 0 to 255
  if (motorSpeedA > 255) {
    motorSpeedA = 255;
    digitalWrite(H1in1, LOW);
    digitalWrite(H1in2, HIGH);
  }
  if (motorSpeedB > 255) {
    motorSpeedB = 255;
    digitalWrite(H1in3, HIGH);
    digitalWrite(H1in4, LOW);
  }
  if (motorSpeedC > 255) {
    motorSpeedC = 255;
    digitalWrite(H2in1, LOW);
    digitalWrite(H2in2, HIGH);
  }
  if (motorSpeedD > 255) {
    motorSpeedD = 255;
    digitalWrite(H2in3, HIGH);
    digitalWrite(H2in4, LOW);
  }
}

void moveLeft_Remote() {
  // Confine the range from 0 to 255
    digitalWrite(H1in1, LOW);
    digitalWrite(H1in2, HIGH);
    digitalWrite(H1in3, HIGH);
    digitalWrite(H1in4, LOW);

    digitalWrite(H2in1, LOW);
    digitalWrite(H2in2, HIGH);
    digitalWrite(H2in3, HIGH);
    digitalWrite(H2in4, LOW);

    motorSpeedAR = PWMAngular;
    motorSpeedBR = PWMAngular;
    motorSpeedCR = PWMAngular;
    motorSpeedDR = PWMAngular;

}

void moveRight() {
  // Convert the increasing X-axis readings from 550 to 1023 into 0 to 255 value
  int xMapped = map(xAxis, 550, 1023, 0, 255);
  // Move right - decrease right motor speed, increase left motor speed
  motorSpeedA = 256;
  motorSpeedB = 256;
  motorSpeedC = 256;
  motorSpeedD = 256;
  // Confine the range from 0 to 255
  if (motorSpeedA > 255) {
    motorSpeedA = 255;
    digitalWrite(H1in1, HIGH);
    digitalWrite(H1in2, LOW);
  }
  if (motorSpeedB > 255) {
    motorSpeedB = 255;
    digitalWrite(H1in3, LOW);
    digitalWrite(H1in4, HIGH);
  }
  if (motorSpeedC > 255) {
    motorSpeedC = 255;
    digitalWrite(H2in1, HIGH);
    digitalWrite(H2in2, LOW);
  }
  if (motorSpeedD > 255) {
    motorSpeedD = 255;
    digitalWrite(H2in3, LOW);
    digitalWrite(H2in4, HIGH);
  }
  Serial.println("Aqui ando");
}

void moveRight_Remote() {

    digitalWrite(H1in1, HIGH);
    digitalWrite(H1in2, LOW);
    digitalWrite(H1in3, LOW);
    digitalWrite(H1in4, HIGH);

    digitalWrite(H2in1, HIGH);
    digitalWrite(H2in2, LOW);
    digitalWrite(H2in3, LOW);
    digitalWrite(H2in4, HIGH);

    motorSpeedAR = PWMAngular;
    motorSpeedBR = PWMAngular;
    motorSpeedCR = PWMAngular;
    motorSpeedDR = PWMAngular;
}

void joystickStandby() {
  // If joystick stays in middle, the motors are not moving
  motorSpeedA = 0;
  motorSpeedB = 0;
  motorSpeedC = 0;
  motorSpeedD = 0;
}

void preventBuzzing() {
  // Prevent buzzing at low speeds (Adjust according to your motors. My motors couldn't start moving if PWM value was below value of 70)
  if (motorSpeedA < 70) { motorSpeedA = 0; }
  if (motorSpeedB < 70) { motorSpeedB = 0; }
  if (motorSpeedC < 70) { motorSpeedC = 0; }
  if (motorSpeedD < 70) { motorSpeedD = 0; }
}

void sendOutputMotors() {
  // Send the output to enable the motors:
  analogWrite(H1enA, motorSpeedA);  // Send PWM signal to motor A
  analogWrite(H1enB, motorSpeedB);  // Send PWM signal to motor B
  analogWrite(H2enA, motorSpeedC);  // Send PWM signal to motor C
  analogWrite(H2enB, motorSpeedD);  // Send PWM signal to motor D
}

void sendOutputMotors_Remote() {
  // Send the output to enable the motors:
  analogWrite(H1enA, motorSpeedAR);  // Send PWM signal to motor A
  analogWrite(H1enB, motorSpeedBR);  // Send PWM signal to motor B
  analogWrite(H2enA, motorSpeedCR);  // Send PWM signal to motor C
  analogWrite(H2enB, motorSpeedDR);  // Send PWM signal to motor D
}

void setup() {
  pinMode(H1enA, OUTPUT);
  pinMode(H1enB, OUTPUT);
  pinMode(H1in1, OUTPUT);
  pinMode(H1in2, OUTPUT);
  pinMode(H1in3, OUTPUT);
  pinMode(H1in4, OUTPUT);
  pinMode(H2enA, OUTPUT);
  pinMode(H2enB, OUTPUT);
  pinMode(H2in1, OUTPUT);
  pinMode(H2in2, OUTPUT);
  pinMode(H2in3, OUTPUT);
  pinMode(H2in4, OUTPUT);

  // Joystick manual control:
  pinMode(xAxis, INPUT);
  pinMode(yAxis, INPUT);
  pinMode(button, INPUT_PULLUP);    // Using a pullup instead of having the physical resistance in the circuit

  // LED for manual control:
  pinMode(LED, OUTPUT);


  // Initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);        

  //Serial COM-
  Serial.begin(57600);

  nh.initNode();
  nh.subscribe(sub);

}

void loop() {
  
  readJoystick();

  // Determine if the manual control is activated:
  if ( enable ) {
    // Turn on the built-in LED
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(LED, HIGH);

    // Determine the movement:
    if ( Mov_Y < 460 )      { moveBackward();    }
    else if ( Mov_Y > 570 ) { moveForward();     }
    else if ( Mov_X < 460 ) { moveLeft();        }
    else if ( Mov_X > 570 ) { moveRight();       }
    else                    { joystickStandby(); } 

     sendOutputMotors();   
  }
  else {
    // Turn off the built-in LED:
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(LED, LOW);
    if (MoveFwd == 1){
      moveForward_Remote();
    }
    else if (MoveBck == 1){
      moveBackward_Remote();  
    }
    else if (RotateL == 1){
      moveLeft_Remote();  
    }
    else if(RotateR == 1){
      moveRight_Remote();
    }

     sendOutputMotors_Remote();
  }

  //preventBuzzing();

  nh.spinOnce();
  delay(1);
  
}
